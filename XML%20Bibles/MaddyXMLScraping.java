package myjava;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nejacob
 */
public class MyJava {

 
	public static void main(String[] args) throws Exception{
 
	  SAXBuilder builder = new SAXBuilder();
	  BufferedWriter br = new BufferedWriter(new FileWriter(new File("C:\\Users\\nejacob\\Desktop\\helloBibleProject\\output.sql")));
	  File xmlFile = new File("C:\\Users\\nejacob\\Desktop\\helloBibleProject\\kjv.xml");
 
	  try {
 
		Document document = (Document) builder.build(xmlFile);
		Element rootNode = document.getRootElement();
		String book = null, chapter = null, verse=null;
		int total=0;
		int bookwise=0;
		List list = rootNode.getChildren("book");
		for (int i = 0; i < list.size(); i++) {
		   Element bookelem = (Element) list.get(i);
		   book = bookelem.getAttribute("name").getValue();
		   List chapters = bookelem.getChildren("chapter");
		   for(int j=0; j<chapters.size();j++){
			   Element chaptelem = (Element) chapters.get(j);
			   chapter = chaptelem.getAttribute("name").getValue();
			   List verses = chaptelem.getChildren("verse");
			   for(int k=0;k<verses.size();k++){
				   Element verseelem = (Element) verses.get(k);
				   verse = verseelem.getAttributeValue("name");
				   total++;
				   bookwise++;
                                   String verseText = verseelem.getText();
                                   verseText = verseText.replace("'", "''");
				   br.write("\nINSERT INTO  bible (id ,book,chapter,verse) VALUES (NULL ,'"+ book+"', '"+chapter +"', '"+verse+"');");
				  // UPDATE `bible1` SET NIV="NIV in the beginning" where `book`=Genesis and `chapter`=1 and `verse`=1

				   //System.out.println(total +"--"+ book +"--("+i +")--"+chapter +"--("+j+")--"+verse +"--("+k+")--"+verseelem.getText());
			   }
		   }
		   System.out.println(book+"--"+bookwise);
		   bookwise=0;
		   
		}
		System.out.println("Total: "+total);
	  } catch (IOException io) {
		System.out.println(io.getMessage());
	  } catch (JDOMException jdomex) {
		System.out.println(jdomex.getMessage());
	  }
	  finally{
		  br.flush();
	  }
	}
}